<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	
	<title>Appium -- Start testing your mobile App</title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.html"><p class="tagline">Diesel</p></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="index.html">HOME</a></li>
					<li><a href="request.html">TEST REQUEST</a></li>
					<li><a href="result.html">TEST RESULT</a></li>
					<li><a class="btn" href="signin.html">SIGN IN / SIGN UP</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">
		
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">Submit result</li>
		</ol>

		<div class="row">
			
			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-left">

				<div class="row widget">
					<div class="col-xs-12">
						<h4>Appium: Your best tool for testing.</h4>
						<p>Appium is an open source test automation framework for use with native, hybrid and mobile web apps. It drives iOS and Android apps using the WebDriver protocol.</p>
					</div>
				</div>
				<div class="row widget">
					<div class="col-xs-12">
						<h4>Use Appium</h4>
						<p><img src="assets/images/1.png" alt=""></p>
					</div>
				</div>

			</aside>
			<!-- /Sidebar -->

			<!-- Article main content -->
			<article class="col-md-8 maincontent">
			<header class="page-header">
					<% String fin=(String)session.getAttribute("fin"); %>
					<% String pro=(String)session.getAttribute("pro"); %>
					<% String failed=(String)session.getAttribute("failed"); %>
					<h1 class="page-title">You have <%= fin %> finished jobs, <%= failed %> failed jobs and <%= pro %> processing jobs.</h1>
					<table border=1 cellpadding=5>
          				<tr>
            				<th>JobID</th>
            				<th>URL</th>
            				<th>Type</th>
            				<th>Status</th>
            				<th>Result</th>
          				</tr>
					<%@ page import = "com.mongodb.*" %>
					<%  MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
						//MongoCon mgc = new MongoCon();
						//DB db = mgc.setPrimary();
						DB db = mongoClient.getDB("diesel");
						DBCollection job_coll = db.getCollection("job");
						BasicDBObject andQuery = new BasicDBObject();
	    				List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
	    				String user=(String)session.getAttribute("user");
	    				obj.add(new BasicDBObject("user", user));
	    				obj.add(new BasicDBObject("status", "2"));
	    				andQuery.put("$and", obj);
	    		
	    				DBCursor cursor = job_coll.find(andQuery);
	    				while (cursor.hasNext()) {
	    					//System.out.println(cursor.next());
	        				DBObject res = cursor.next();
	        				int JobID = (Integer)res.get("jobID");
	        				String URL = (String)res.get("url");
	        				String status = (String)res.get("status");
	        				//System.out.println("JobID: "+res.get("jobID")+" URL: "+ res.get("url")+" Status: "+res.get("status"));%>
	        			<tr>
            				<td><%= JobID %></td>
            				<td><%= URL %></td>
            				<td>click</td>
            				<td>processing</td>
            				<td></td>
          				</tr>
	    			<%
	    			}
	    			%>
	    			<%  obj.clear();
	    				obj.add(new BasicDBObject("user", user));
	    				obj.add(new BasicDBObject("status", "1"));
	    				andQuery.clear();
	    				andQuery.put("$and", obj);
	    		
	    				cursor = job_coll.find(andQuery);
	    				while (cursor.hasNext()) {
	    					//System.out.println(cursor.next());
	        				DBObject res = cursor.next();
	        				int JobID = (Integer)res.get("jobID");
	        				String URL = (String)res.get("url");
	        				String status = (String)res.get("status");
	        				//System.out.println("JobID: "+res.get("jobID")+" URL: "+ res.get("url")+" Status: "+res.get("status"));%>
	        			<tr>
            				<td><%= JobID %></td>
            				<td><%= URL %></td>
            				<td>click</td>
            				<td>finished</td>
            				<td><a href="picture.jsp?id=<%= JobID %>">view</a></td>
          				</tr>
	    			<%
	    			}
	    			%>
	    			<%  obj.clear();
	    				obj.add(new BasicDBObject("user", user));
	    				obj.add(new BasicDBObject("status", "-1"));
	    				andQuery.clear();
	    				andQuery.put("$and", obj);
	    		
	    				cursor = job_coll.find(andQuery);
	    				while (cursor.hasNext()) {
	    					//System.out.println(cursor.next());
	        				DBObject res = cursor.next();
	        				int JobID = (Integer)res.get("jobID");
	        				String URL = (String)res.get("url");
	        				String status = (String)res.get("status");
	        				//System.out.println("JobID: "+res.get("jobID")+" URL: "+ res.get("url")+" Status: "+res.get("status"));%>
	        			<tr>
            				<td><%= JobID %></td>
            				<td><%= URL %></td>
            				<td>click</td>
            				<td>failed</td>
          				</tr>
	    			<%
	    			}
	    			%>
	    			</table>
					</header>
				

			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p>+234 23 9873237<br>
								<a href="mailto:#">some.email@somewhere.com</a><br>
								<br>
								234 Hidden Pond Road, Ashland City, TN 37015
							</p>	
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Follow me</h3>
						<div class="widget-body">
							<p class="follow-me-icons">
								<a href=""><i class="fa fa-twitter fa-2"></i></a>
								<a href=""><i class="fa fa-dribbble fa-2"></i></a>
								<a href=""><i class="fa fa-github fa-2"></i></a>
								<a href=""><i class="fa fa-facebook fa-2"></i></a>
							</p>	
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">Text widget</h3>
						<div class="widget-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, dolores, quibusdam architecto voluptatem amet fugiat nesciunt placeat provident cumque accusamus itaque voluptate modi quidem dolore optio velit hic iusto vero praesentium repellat commodi ad id expedita cupiditate repellendus possimus unde?</p>
							<p>Eius consequatur nihil quibusdam! Laborum, rerum, quis, inventore ipsa autem repellat provident assumenda labore soluta minima alias temporibus facere distinctio quas adipisci nam sunt explicabo officia tenetur at ea quos doloribus dolorum voluptate reprehenderit architecto sint libero illo et hic.</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<a href="about.html">About</a> |
								<a href="sidebar-right.html">Sidebar</a> |
								<a href="contact.html">Contact</a> |
								<b><a href="signup.html">Sign up</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; 2014, Your name. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a> 
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
</body>
</html>