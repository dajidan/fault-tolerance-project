import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.Binary;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

public class MongoCon {
	private String defaultprimary = "localhost";
	private String primary = "localhost";
	private String secondary1 = "128.237.179.42";
	private String secondary2 = "128.2.47.51";
	
	private MongoClient mongoClient = new MongoClient( primary , 27017 );
	private DB db = mongoClient.getDB("diesel");
	
	private DBCollection coll = db.getCollection("user");
	private DBCollection result_coll = db.getCollection("result");
	private DBCollection job_coll = db.getCollection("job");
	
	public DBCollection getJobCollection(){
		setPrimary();
		return this.job_coll;
	}
	
	public DB setPrimary(){
		//mongoClient.close();
		mongoClient = new MongoClient(primary,27017);
		db = mongoClient.getDB("diesel");
		DBCollection testcoll = db.getCollection("testconn");
		try{
			testcoll.insert(new BasicDBObject("testconn","ke"));
		}
		catch(MongoException e){
			e.printStackTrace();
			MongoClient client2 = new MongoClient(secondary1,27017);
			DB db2 = client2.getDB("diesel");
			testcoll = db2.getCollection("testconn");
			try{
				testcoll.insert(new BasicDBObject("testconn","ke"));
			}
			catch(MongoException e2){
				primary = secondary2;
				System.out.println("Current primary: "+primary);
				//mongoClient.close();
				mongoClient = new MongoClient( primary , 27017 );
				db = mongoClient.getDB("diesel");
				coll = db.getCollection("user");
				result_coll = db.getCollection("result");
				job_coll = db.getCollection("job");
				return db;
			}
			primary = secondary1;
			System.out.println("Current primary: "+primary);
			//mongoClient.close();
			mongoClient = new MongoClient( primary , 27017 );
			db = mongoClient.getDB("diesel");
			coll = db.getCollection("user");
			result_coll = db.getCollection("result");
			job_coll = db.getCollection("job");
			return db;
		}
		System.out.println("Current primary: "+primary);
		return db;
		
	}
	private String SampleImg = "SampleImage1";
	File imageFile = new File("/Users/kewang/Workspaces/MyEclipse 10/Diesel/diesel/WebRoot/test.png");
	
    public void insertUser(String email, String passwd){
    	BasicDBObject doc = new BasicDBObject("email", email)
        .append("passwd",passwd);
    	setPrimary();
    	coll.insert(doc);
    }
    
    
    public String checkUser(String email){
    	setPrimary();
    	BasicDBObject query = new BasicDBObject("email", email);
    	DBObject passwd = coll.findOne(query);
    	if(passwd==null)
    		return "";
    	return (String) passwd.get("passwd");
    	
    }
    
    public void insertJob(String user, int jobid,String url){
    	setPrimary();
    	BasicDBObject job = new BasicDBObject("user",user).append("jobID", jobid).append("url", url)
        .append("status","0");
    	
    	job_coll.insert(job);
    }
    
    /*public void insert(String empname, String filename, DBCollection collection)
    {
        try
        {
            File imageFile = new File(filename);
            FileInputStream f = new FileInputStream(imageFile);
 
            byte b[] = new byte[f.available()];
            f.read(b);
 
            Binary data = new Binary(b);
            BasicDBObject o = new BasicDBObject();
            o.append("name",empname).append("photo",data);
            collection.insert(o);
            System.out.println("Inserted record.");
 
            f.close();
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    void retrieve(String name, String filename, DBCollection collection)
    {
        byte c[];
        try
        {
            DBObject obj = collection.findOne(new BasicDBObject("name", name));
            String n = (String)obj.get("name");
            c = (byte[])obj.get("photo");
            FileOutputStream fout = new FileOutputStream(filename);
            fout.write(c);
            fout.flush();
            System.out.println("Photo of "+name+" retrieved and stored at "+filename);
            fout.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void addphoto(String newFileName) throws IOException{  
        String filename = "/Users/kewang/Workspaces/MyEclipse 10/Diesel/diesel/WebRoot/test.jpg";
        String empname = newFileName;
         
        //Inserts a record with name = empname and photo 
        //  specified by the filepath 
        //
        insert(empname,filename,result_coll);
         
        String destfilename = "/Users/kewang/Workspaces/MyEclipse 10/Diesel/diesel/WebRoot/test.png";
        // Retrieves record where name = empname, including his photo. 
        // Retrieved photo is stored at location filename 
        //
        retrieve(empname, destfilename, result_coll);
    }*/
}
