import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import message.Job;
import message.Message;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

@MultipartConfig
public class Submit extends HttpServlet {
	
    private MongoCon mgc = new MongoCon();
	/**
	 * Constructor of the object.
	 */
	public Submit() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the GET method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}
	
	private String username,id,type,url;
	
	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String user = request.getParameter("username");
		String url = request.getParameter("url");
		
		DatagramSocket socket = new DatagramSocket();
		DatagramPacket packet = new DatagramPacket(new byte[0], 0, InetAddress.getByName("128.237.192.128"), 8008);
        
		//these output streams will send information to the server.
    	ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
    	ObjectOutputStream objectStream = new ObjectOutputStream(byteArrayStream);
    	
    	Random rand = new Random();
    	int jobID = rand.nextInt(10000);
    	System.out.println(jobID);
    	Job job = new Job(String.valueOf(jobID), "click", url, 1, user);
    	try {
			Message msg = job.generateMessage();
			objectStream.writeObject(msg);
        	//transform the data to bytes in order to use UDP
        	byte[] arr = byteArrayStream.toByteArray();
        	packet.setData(arr);
        	long start = System.currentTimeMillis();
        	socket.send(packet);
        	
        	mgc.insertJob(user,jobID,url);
        	//mgc.testCon();
        	//mgc.addphoto("kewang");
        	HttpSession session=request.getSession();
			session.setAttribute("jobid", String.valueOf(jobID));
			response.sendRedirect("submited.jsp");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void processFormField(FileItem item){
		if (item.isFormField()) {
		    String name = item.getFieldName();
		    String value = item.getString();
		    if(name.equals("username")){
		    	username = value;
		    }
		    else if(name.equals("device")){
		    	type = value;
		    }
		    else if(name.equals("url")){
		    	url = value;
		    }
		    System.out.println(name+" "+value);
		}
	}
	public void processUploadedFile(FileItem item){
		if (!item.isFormField()) {
		    String fieldName = item.getFieldName();
		    String fileName = item.getName();
		    String contentType = item.getContentType();
		    boolean isInMemory = item.isInMemory();
		    long sizeInBytes = item.getSize();
		    int size = (int)(sizeInBytes/1024);
		    System.out.print(fieldName+" "+fileName+" size: "+size+" kb.");
		}
	}
	
	
	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
