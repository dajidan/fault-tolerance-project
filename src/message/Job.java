package message;

import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

public class Job{
    private String type;  // "scroll"  "click"
    private String value;   // like url
    private String jobId;   // every job will be assigned a random but unique id
    private int userId;
    private String username;

    private String status;

    public Job(String jobId, String type, String value, int uid, String username){
        this.type = type;
        this.value = value;
        userId = uid;
        this.username = username;
        status = "waiting";
        this.jobId = jobId;
    }
    
    public Job(JSONObject jobjson) throws JSONException{
        this.jobId = jobjson.getString("jobid");
        this.type = jobjson.getString("type");
        this.value = jobjson.getString("url");
        this.userId = jobjson.getInt("userId");
        this.username = jobjson.getString("userName");
    }

    public String getJobId(){
        return jobId;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public Message generateMessage() throws JSONException{
        JSONObject json = new JSONObject();
        json.put("jobid",jobId);
        json.put("type",type);
        json.put("url", value);
        json.put("userId", userId);
        json.put("userName", username);

        Message command = new Message(2, json.toString());
        return command;
    }

    public JSONObject getJobInJson() throws JSONException{
        JSONObject json = new JSONObject();
        json.put("jobid",jobId);
        json.put("type",type);
        json.put("url", value);
        json.put("userId", userId);
        json.put("userName", username);
        return json;
    }
}