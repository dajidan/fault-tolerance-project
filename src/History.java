import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@MultipartConfig
public class History extends HttpServlet {
	private MongoCon mgc = new MongoCon();
	
	OutputStream os;
            
	/**
	 * Constructor of the object.
	 */
	public History() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the GET method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}
	
	private String username,id,type,url;
	
	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		DBCollection result_coll = mgc.getJobCollection();
		
		BasicDBObject andQuery = new BasicDBObject();
	    List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
	    
	    obj.add(new BasicDBObject("user", username));
	    obj.add(new BasicDBObject("status", "2"));
	    andQuery.put("$and", obj);
	    
	    DBCursor cursor = result_coll.find(andQuery);
	    int processing = 0;
	    while (cursor.hasNext()) {
	    	processing = processing+1;
	        //System.out.println(cursor.next());
	        DBObject res = cursor.next();
	        System.out.println("JobID: "+res.get("jobID")+" URL: "+ res.get("url")+" Status: "+res.get("status"));
	    }
	    
	    
	    obj.clear();
	    obj.add(new BasicDBObject("user", username));
	    obj.add(new BasicDBObject("status", "1"));
	    andQuery.clear();
	    andQuery.put("$and", obj);
	    cursor = result_coll.find(andQuery);
	    int finished = 0;
	    while (cursor.hasNext()) {
	    	finished = finished+1;
	        System.out.println(cursor.next());
	    }
	    
	    obj.clear();
	    obj.add(new BasicDBObject("user", username));
	    obj.add(new BasicDBObject("status", "-1"));
	    andQuery.clear();
	    andQuery.put("$and", obj);
	    cursor = result_coll.find(andQuery);
	    int failed = 0;
	    while (cursor.hasNext()) {
	    	failed = failed+1;
	        System.out.println(cursor.next());
	    }
	    HttpSession session=request.getSession();
	    session.setAttribute("user", ""+username);
	    session.setAttribute("pro", ""+processing);
		session.setAttribute("fin", ""+finished);
		session.setAttribute("failed",""+failed);
		response.sendRedirect("history.jsp");
	  

	}
	
	public void processFormField(FileItem item){
		if (item.isFormField()) {
		    String name = item.getFieldName();
		    String value = item.getString();
		    if(name.equals("username")){
		    	username = value;
		    }
		    else if(name.equals("device")){
		    	type = value;
		    }
		    else if(name.equals("url")){
		    	url = value;
		    }
		    System.out.println(name+" "+value);
		}
	}
	public void processUploadedFile(FileItem item){
		if (!item.isFormField()) {
		    String fieldName = item.getFieldName();
		    String fileName = item.getName();
		    String contentType = item.getContentType();
		    boolean isInMemory = item.isInMemory();
		    long sizeInBytes = item.getSize();
		    int size = (int)(sizeInBytes/1024);
		    System.out.print(fieldName+" "+fileName+" size: "+size+" kb.");
		}
	}
	
	
	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
